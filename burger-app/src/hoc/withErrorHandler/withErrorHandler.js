import React from 'react';
import Modal from '../../components/UI/Modal/Modal';
import Auxiliary from '../Auxiliary/Auxiliary';
import useHttpErrorHandler from '../../hooks/http-error-handler'

const withErrorHandler = (WrappedComponent, axios) => {
  return props => {
    const [error, clearError] = useHttpErrorHandler(axios)

    return (
      <Auxiliary>
        <Modal show={error} modalClosed={clearError}>
          {error ? error.message : null}
        </Modal>
        <WrappedComponent {...props} />
      </Auxiliary>
    )
  }
  // return class extends React.Component {
  //   constructor(props) {
  //     super(props)
  //     this.state = {
  //       error: null
  //     }
  //     this.reqInterceptor = axios.interceptors.response.use(res => res, error => {
  //       this.setState({ error: error })
  //     })
  //     this.resInterceptor = axios.interceptors.request.use(req => {
  //       this.setState({ error: null })
  //       return req
  //     })
  //   }
  //
  //   componentWillUnmount() {
  //     axios.interceptors.request.eject(this.reqInterceptor)
  //     axios.interceptors.response.eject(this.resInterceptor)
  //   }
  //
  //   // componentDidMount() {
  //   //   axios.interceptors.response.use(res => res, error => {
  //   //     this.setState({ error: error })
  //   //   })
  //   //   axios.interceptors.request.use(req => {
  //   //     this.setState({ error: null })
  //   //     return req
  //   //   })
  //   // }
  //
  //   errorConfirmedHandler = () => {
  //     this.setState({ error: null })
  //   }
  //
  //   render () {
  //     return (
  //       <Auxiliary>
  //         <Modal
  //           show={this.state.error}
  //           modalClosed={this.errorConfirmedHandler}
  //         >
  //           {this.state.error ? this.state.error.message : null}
  //         </Modal>
  //         <WrappedComponent {...this.props} />
  //       </Auxiliary>
  //     )
  //   }
  // }
}

export default withErrorHandler;
