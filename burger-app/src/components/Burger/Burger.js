import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import classes from './Burger.module.css';

const burger = props => {
  // Obtains the props passed through BurgerIngredint ingredients state and
  // creates an array from the object that registers the the first value of
  // each element in the state as an array that increases size depending on
  // the value of the element. So if cheese: 2 it creates 2 arrays of cheese
  // to render.
  let transformedIngredients = Object.keys(props.ingredients).map(igKey => {
    return [...Array(props.ingredients[igKey])].map((_, i) => {
      return <BurgerIngredient key={igKey + i} type={igKey} />
    })
    // Reduce flattens the array recieving the array and an element. so in
    // this case if the array is empty, it flattens the array and adds the
    // empty elements at the end of the array, in order to measure length.
    // Also the first element is declared empty as [] sets.
  }).reduce((arr, el) => {
    return arr.concat(el)
  }, [])

  // The array of arrays that was the ingredients state was flattened in order
  // measure the length of the array and define a case when there are no 
  // ingredients, to display instructions.
  if (transformedIngredients.length === 0) {
    transformedIngredients = <p>Please start adding ingredients</p>
  }

  return (
    <div className={classes.Burger}>
      <BurgerIngredient type="breadTop" />
      {transformedIngredients}
      <BurgerIngredient type="breadBottom" />
    </div>
  )
}

export default burger;
