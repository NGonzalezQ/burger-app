import React from 'react';
import Burger from '../../Burger/Burger';
import CustomButton from '../../UI/CustomButton/CustomButton';
import classes from './CheckoutSummary.module.css';

const checkoutSummary = props => {
  return (
    <div className={classes.CheckoutSummary}>
      <h1>We hope it tastes</h1>
      <div style={{width: '100%', margin: 'auto'}}>
        <Burger ingredients={props.ingredients} />
      </div>
      <CustomButton
        customBtnType="Danger"
        clicked={props.checkoutCanceled}
      >
        CANCEL
      </CustomButton>
      <CustomButton
        customBtnType="Success"
        clicked={props.checkoutContinued}
      >
        CONTINUE
      </CustomButton>
    </div>
  )
}

export default checkoutSummary;
