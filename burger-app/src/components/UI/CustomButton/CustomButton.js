import React from 'react';
import classes from './CustomButton.module.css';

const customButton = props => (
  <button
    disabled={props.disabled}
    className={[classes.CustomButton, classes[props.customBtnType]].join(' ')}
    onClick={props.clicked}
  >
    {props.children}
  </button>
)

export default customButton;
