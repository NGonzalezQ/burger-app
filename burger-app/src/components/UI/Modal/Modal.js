import React from 'react';
import Auxiliary from '../../../hoc/Auxiliary/Auxiliary';
import Backdrop from '../Backdrop/Backdrop';
import classes from './Modal.module.css';

// React.memo allows to memoize the functional component so it only updates
// if the component itself requires an update, equivalent to conver this
// method to a class and add a shouldComponentUpdate function to condition
// the rendering or using a Pure.Component, although Pure.Component runs
// more checks in comparison.

const modal = props => (
  <Auxiliary>
    <Backdrop show={props.show} clicked={props.modalClosed} />
    <div
      className={classes.Modal}
      style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'
      }}
    >
      {props.children}
    </div>
  </Auxiliary>
)

export default React.memo(
  modal,
  (prevProps, nextProps) =>
    nextProps.show === prevProps.show && 
    nextProps.children === prevProps.children
);
