import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import Auxiliary from '../../hoc/Auxiliary/Auxiliary'
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/../hoc/withErrorHandler/withErrorHandler';
import * as actions from '../../store/actions/index';

const BurgerBuilder = props => {
  const [purchasing, setPurchasing] = useState(false)

  useEffect(() => {
    props.onInitIngredients()
  }, [])

  const updatePurchaseState = (ingredients) => {
    const sum = Object.keys(ingredients)
      .map(igKey => {
        return ingredients[igKey]
      })
      .reduce((sum, el) => {
        return sum + el
      }, 0)
    return sum > 0
  }

  const purchaseHandler = () => {
    if (props.isAuthenticated) {
      setPurchasing(true)
    } else {
      props.onSetAuthRedirectPath('/checkout')
      props.history.push('/auth')
    }
  }

  const purchaseCancelHandler = () => {
    setPurchasing(false)
  }

  const purchaseContinueHandler = () => {
    props.onInitPurchase()
    props.history.push('/checkout')
  }

  const disabledInfo = {
    ...props.ings
  }
  for (let key in disabledInfo) {
    disabledInfo[key] = disabledInfo[key] <= 0
  }

  let orderSummary = null

  let burger = props.error ? <p>Load problems with ingredients</p> : <Spinner />
  if (props.ings) {
    burger = (
      <Auxiliary>
        <Burger ingredients={props.ings} />
        <BuildControls
          ingredientAdded={props.onIngredientAdded}
          ingredientRemoved={props.onIngredientRemoved}
          disabled={disabledInfo}
          purchasable={updatePurchaseState(props.ings)}
          ordered={purchaseHandler}
          isAuth={props.isAuthenticated}
          price={props.price}
        />
      </Auxiliary>
    )
    orderSummary =
      <OrderSummary
        ingredients={props.ings}
        price={props.price}
        purchaseCanceled={purchaseCancelHandler}
        purchaseContinued={purchaseContinueHandler}
      />
  }

  return(
    <Auxiliary>
      <Modal
        show={purchasing}
        modalClosed={purchaseCancelHandler}
      >
        {orderSummary}
      </Modal>
      {burger}
    </Auxiliary>
  )
}

// export class BurgerBuilder extends React.Component {
//   state = {
//     purchasing: false,
//   }
//
//   componentDidMount () {
//     this.props.onInitIngredients()
//   }
//
//   updatePurchaseState = (ingredients) => {
//     const sum = Object.keys(ingredients)
//       .map(igKey => {
//         return ingredients[igKey]
//       })
//       .reduce((sum, el) => {
//         return sum + el
//       }, 0)
//     return sum > 0
//   }
//
//   // addIngredientHandler = type => {
//   //   const oldCount = this.state.ingredients[type]
//   //   const updatedCount = oldCount + 1
//   //   const updatedIngredients = {
//   //     ...this.state.ingredients
//   //   }
//   //   updatedIngredients[type] = updatedCount
//   //   const priceAddition = INGREDIENT_PRICES[type]
//   //   const oldPrice = this.state.totalPrice
//   //   const newPrice = oldPrice + priceAddition
//   //   this.setState({
//   //     totalPrice: newPrice,
//   //     ingredients: updatedIngredients
//   //   })
//   //   this.updatePurchaseState(updatedIngredients)
//   // }
//   //
//   // removeIngredientHandler = type => {
//   //   const oldCount = this.state.ingredients[type]
//   //   if (oldCount <= 0) {
//   //     return
//   //   }
//   //   const updatedCount = oldCount - 1
//   //   const updatedIngredients = {
//   //     ...this.state.ingredients
//   //   }
//   //   updatedIngredients[type] = updatedCount
//   //   const priceDeduction = INGREDIENT_PRICES[type]
//   //   const oldPrice = this.state.totalPrice
//   //   const newPrice = oldPrice - priceDeduction
//   //   this.setState({
//   //     totalPrice: newPrice,
//   //     ingredients: updatedIngredients
//   //   })
//   //   this.updatePurchaseState(updatedIngredients)
//   // }
//
//   purchaseHandler = () => {
//     if (this.props.isAuthenticated) {
//       this.setState({ purchasing: true })
//     } else {
//       this.props.onSetAuthRedirectPath('/checkout')
//       this.props.history.push('/auth')
//     }
//   }
//
//   purchaseCancelHandler = () => {
//     this.setState({ purchasing: false })
//   }
//
//   purchaseContinueHandler = () => {
//     // const queryParams = []
//     // for (let i in this.state.ingredients) {
//     //   queryParams.push(
//     //     encodeURIComponent(i) + '=' + encodeURIComponent(this.state.ingredients[i])
//     //   )
//     // }
//     // queryParams.push('price=' + this.state.totalPrice)
//     // const queryString = queryParams.join('&')
//     this.props.onInitPurchase()
//     this.props.history.push('/checkout')
//       // pathname: '/checkout',
//       // search: '?' + queryString
//   }
//
//   render() {
//     const disabledInfo = {
//       ...this.props.ings
//     }
//     for (let key in disabledInfo) {
//       disabledInfo[key] = disabledInfo[key] <= 0
//     }
//     let orderSummary = null
//
//     let burger = this.props.error ? <p>Load problems with ingredients</p> : <Spinner />
//
//     if (this.props.ings) {
//       burger = (
//         <Auxiliary>
//           <Burger ingredients={this.props.ings} />
//           <BuildControls
//             ingredientAdded={this.props.onIngredientAdded}
//             ingredientRemoved={this.props.onIngredientRemoved}
//             disabled={disabledInfo}
//             purchasable={this.updatePurchaseState(this.props.ings)}
//             ordered={this.purchaseHandler}
//             isAuth={this.props.isAuthenticated}
//             price={this.props.price}
//           />
//         </Auxiliary>
//       )
//       orderSummary =
//         <OrderSummary
//           ingredients={this.props.ings}
//           price={this.props.price}
//           purchaseCanceled={this.purchaseCancelHandler}
//           purchaseContinued={this.purchaseContinueHandler}
//         />
//     }
//     // {salad: true, meat: false, ...}
//
//     return(
//       <Auxiliary>
//         <Modal
//           show={this.state.purchasing}
//           modalClosed={this.purchaseCancelHandler}
//         >
//           {orderSummary}
//         </Modal>
//         {burger}
//       </Auxiliary>
//     )
//   }
// }

const mapStateToProps = state => {
  return {
    ings: state.burgerBuilder.ingredients,
    price: state.burgerBuilder.totalPrice,
    error: state.burgerBuilder.error,
    isAuthenticated: state.auth.token !== null
  }
}

const mapDispatchtoProps = dispatch => {
  return {
    onIngredientAdded: (ingName) =>
      dispatch(actions.addIngredient(ingName)),
    onIngredientRemoved: (ingName) =>
      dispatch(actions.removeIngredient(ingName)),
    onInitIngredients: () => dispatch(actions.initIngredients()),
    onInitPurchase: () => dispatch(actions.purchaseInit()),
    onSetAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
  }
}

export default connect(mapStateToProps, mapDispatchtoProps)(withErrorHandler(BurgerBuilder, axios));
