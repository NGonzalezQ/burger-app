import React, { useState, useEffect} from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import Input from '../../components/UI/Input/Input';
import CustomButton from '../../components/UI/CustomButton/CustomButton';
import classes from './Auth.module.css';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import { updateObject, checkValidity } from '../../shared/utility';

const Auth = props => {
  const [authForm, setAuthForm] = useState({
    email: {
      elementType: 'input',
      elementConfig: {
        type: 'email',
        placeholder: 'Mail Address'
      },
      value: '',
      validation: {
        required: true,
        isEmail: true
      },
      valid: false,
      touched: false
    },
    password: {
      elementType: 'input',
      elementConfig: {
        type: 'password',
        placeholder: 'Password'
      },
      value: '',
      validation: {
        required: true,
        minLength: 6
      },
      valid: false,
      touched: false
    }
  })
  const [isSignUp, setIsSignUp] = useState(true)

  useEffect(() => {
    if (!props.buildingBurger && props.authRedirectPath !== '/') {
      props.onSetAuthRedirectPath()
    }
  }, [])

  const inputChangedHandler = (event, controlName) => {
    const updatedControls = updateObject(authForm, {
      [controlName]: updateObject(authForm[controlName], {
        value: event.target.value,
        valid: checkValidity(event.target.value, authForm[controlName].validation),
        touched: true
      })
    })
    // const updatedControls = {
    //   ...this.state.controls,
    //   [controlName]: {
    //     ...this.state.controls[controlName],
    //     value: event.target.value,
    //     valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
    //     touched: true
    //   }
    // }
    setAuthForm(updatedControls)
  }

  const submitHandler = event => {
    event.preventDefault()
    props.onAuth(
      authForm.email.value,
      authForm.password.value,
      isSignUp
    )
  }

  const switchAuthModeHandler = () => {
    setIsSignUp(!isSignUp)
  }

  const formElementsArray = []
  for (let key in authForm) {
    formElementsArray.push({
      id: key,
      config: authForm[key]
    })
  }

  let form = formElementsArray.map(formElement => (
    <Input
      key={formElement.id}
      elementType={formElement.config.elementType}
      elementConfig={formElement.config.elementConfig}
      value={formElement.config.value}
      invalid={!formElement.config.valid}
      shouldValidate={formElement.config.validation}
      touched={formElement.config.touched}
      changed={(event) => inputChangedHandler(event, formElement.id)}
    />
  ))

  if (props.loading) {
    form = <Spinner />
  }

  let errorMessage = null
  if (props.error) {
    errorMessage = (
      <p>{props.error.message}</p>
    )
  }

  let authRedirect = null
  if (props.isAuthenticated) {
    authRedirect = <Redirect to={props.authRedirectPath} />
  }

  return (
    <div className={classes.Auth}>
      {authRedirect}
      {errorMessage}
      <form onSubmit={submitHandler}>
        {form}
        <CustomButton customBtnType="Success">
          SUBMIT
        </CustomButton>
      </form>
      <CustomButton
        clicked={switchAuthModeHandler}
        customBtnType="Danger"
      >
        SWITCH TO {isSignUp ? 'SIGN IN' : 'SIGN UP'}
      </CustomButton>
    </div>
  )
}

// class Auth extends React.Component {
//   state = {
//     controls: {
//       email: {
//         elementType: 'input',
//         elementConfig: {
//           type: 'email',
//           placeholder: 'Mail Address'
//         },
//         value: '',
//         validation: {
//           required: true,
//           isEmail: true
//         },
//         valid: false,
//         touched: false
//       },
//       password: {
//         elementType: 'input',
//         elementConfig: {
//           type: 'password',
//           placeholder: 'Password'
//         },
//         value: '',
//         validation: {
//           required: true,
//           minLength: 6
//         },
//         valid: false,
//         touched: false
//       }
//     },
//     isSignUp: true
//   }
//
//   componentDidMount () {
//     if (!this.props.buildingBurger && this.props.authRedirectPath !== '/') {
//       this.props.onSetAuthRedirectPath()
//     }
//   }
//
//   inputChangedHandler = (event, controlName) => {
//     const updatedControls = updateObject(this.state.controls, {
//       [controlName]: updateObject(this.state.controls[controlName], {
//         value: event.target.value,
//         valid: checkValidity(event.target.value, this.state.controls[controlName].validation),
//         touched: true
//       })
//     })
//     // const updatedControls = {
//     //   ...this.state.controls,
//     //   [controlName]: {
//     //     ...this.state.controls[controlName],
//     //     value: event.target.value,
//     //     valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
//     //     touched: true
//     //   }
//     // }
//     this.setState({ controls: updatedControls })
//   }
//
//   submitHandler = event => {
//     event.preventDefault()
//     this.props.onAuth(
//       this.state.controls.email.value,
//       this.state.controls.password.value,
//       this.state.isSignUp
//     )
//   }
//
//   switchAuthModeHandler = () => {
//     this.setState(prevState => {
//       return {isSignUp: !prevState.isSignUp}
//     })
//   }
//
//   render() {
//     const formElementsArray = []
//     for (let key in this.state.controls) {
//       formElementsArray.push({
//         id: key,
//         config: this.state.controls[key]
//       })
//     }
//
//     let form = formElementsArray.map(formElement => (
//       <Input
//         key={formElement.id}
//         elementType={formElement.config.elementType}
//         elementConfig={formElement.config.elementConfig}
//         value={formElement.config.value}
//         invalid={!formElement.config.valid}
//         shouldValidate={formElement.config.validation}
//         touched={formElement.config.touched}
//         changed={(event) => this.inputChangedHandler(event, formElement.id)}
//       />
//     ))
//
//     if (this.props.loading) {
//       form = <Spinner />
//     }
//
//     let errorMessage = null
//     if (this.props.error) {
//       errorMessage = (
//         // message comes from error.message from firebase, for other API
//         // it needs to be made from scratch
//         <p>{this.props.error.message}</p>
//       )
//     }
//
//     let authRedirect = null
//     if (this.props.isAuthenticated) {
//       authRedirect = <Redirect to={this.props.authRedirectPath} />
//     }
//
//     return (
//       <div className={classes.Auth}>
//         {authRedirect}
//         {errorMessage}
//         <form onSubmit={this.submitHandler}>
//           {form}
//           <CustomButton customBtnType="Success">
//             SUBMIT
//           </CustomButton>
//         </form>
//         <CustomButton
//           clicked={this.switchAuthModeHandler}
//           customBtnType="Danger"
//         >
//           SWITCH TO {this.state.isSignUp ? 'SIGN IN' : 'SIGN UP'}
//         </CustomButton>
//           </div>
//     )
//   }
// }

const mapStateToProps = state => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.token !== null,
    buildingBurger: state.burgerBuilder.building,
    authRedirectPath: state.auth.authRedirectPath
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAuth: (email, password, isSignUp) =>
      dispatch(actions.auth(email, password, isSignUp)),
    onSetAuthRedirectPath: () =>
      dispatch(actions.setAuthRedirectPath('/'))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Auth);
