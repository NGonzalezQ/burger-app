import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://react-burger-udemy-app.firebaseio.com/'
})

export default instance;
